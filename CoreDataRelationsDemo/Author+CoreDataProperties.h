//
//  Author+CoreDataProperties.h
//  CoreDataRelationsDemo
//
//  Created by James Cash on 25-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//  This file was automatically generated and should not be edited.
//

#import "Author+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Author (CoreDataProperties)

+ (NSFetchRequest<Author *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, retain) NSSet<Recipie *> *recipies;
@property (nullable, nonatomic, retain) NSSet<Recipie *> *favourites;

@end

@interface Author (CoreDataGeneratedAccessors)

- (void)addRecipiesObject:(Recipie *)value;
- (void)removeRecipiesObject:(Recipie *)value;
- (void)addRecipies:(NSSet<Recipie *> *)values;
- (void)removeRecipies:(NSSet<Recipie *> *)values;

- (void)addFavouritesObject:(Recipie *)value;
- (void)removeFavouritesObject:(Recipie *)value;
- (void)addFavourites:(NSSet<Recipie *> *)values;
- (void)removeFavourites:(NSSet<Recipie *> *)values;

@end

NS_ASSUME_NONNULL_END
