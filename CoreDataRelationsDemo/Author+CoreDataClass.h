//
//  Author+CoreDataClass.h
//  CoreDataRelationsDemo
//
//  Created by James Cash on 25-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//  This file was automatically generated and should not be edited.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Recipie;

NS_ASSUME_NONNULL_BEGIN

@interface Author : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Author+CoreDataProperties.h"
