//
//  Author+CoreDataProperties.m
//  CoreDataRelationsDemo
//
//  Created by James Cash on 25-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//  This file was automatically generated and should not be edited.
//

#import "Author+CoreDataProperties.h"

@implementation Author (CoreDataProperties)

+ (NSFetchRequest<Author *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Author"];
}

@dynamic name;
@dynamic recipies;
@dynamic favourites;

@end
