//
//  DetailViewController.m
//  CoreDataRelationsDemo
//
//  Created by James Cash on 25-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "DetailViewController.h"
#import "FavedViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)configureView {
    Author* authoredBy = self.detailItem.author;
    NSSet *faved = self.detailItem.favouritedBy;
    self.detailDescriptionLabel.text = [NSString stringWithFormat:@"Written by %@, faved by %ld people", authoredBy.name, [faved count]];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showFavers"]) {
        FavedViewController *dvc = segue.destinationViewController;
        dvc.favers = self.detailItem.favouritedBy;
    }
    
}

#pragma mark - Managing the detail item

@end
