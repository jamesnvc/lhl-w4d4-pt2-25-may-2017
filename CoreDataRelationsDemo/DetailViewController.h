//
//  DetailViewController.h
//  CoreDataRelationsDemo
//
//  Created by James Cash on 25-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Recipie+CoreDataProperties.h"
#import "Author+CoreDataProperties.h"

@interface DetailViewController : UIViewController

@property (strong, nonatomic) Recipie* detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end

