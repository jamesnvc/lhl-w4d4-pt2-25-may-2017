//
//  FavedViewController.h
//  CoreDataRelationsDemo
//
//  Created by James Cash on 25-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Author+CoreDataProperties.h"

@interface FavedViewController : UIViewController

@property (strong,nonatomic) NSSet<Author*>* favers;

@end
