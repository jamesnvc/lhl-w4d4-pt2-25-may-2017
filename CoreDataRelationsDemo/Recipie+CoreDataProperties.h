//
//  Recipie+CoreDataProperties.h
//  CoreDataRelationsDemo
//
//  Created by James Cash on 25-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//  This file was automatically generated and should not be edited.
//

#import "Recipie+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Recipie (CoreDataProperties)

+ (NSFetchRequest<Recipie *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *steps;
@property (nullable, nonatomic, retain) Author *author;
@property (nullable, nonatomic, retain) NSSet<Author *> *favouritedBy;

@end

@interface Recipie (CoreDataGeneratedAccessors)

- (void)addFavouritedByObject:(Author *)value;
- (void)removeFavouritedByObject:(Author *)value;
- (void)addFavouritedBy:(NSSet<Author *> *)values;
- (void)removeFavouritedBy:(NSSet<Author *> *)values;

@end

NS_ASSUME_NONNULL_END
