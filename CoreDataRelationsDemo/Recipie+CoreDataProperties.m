//
//  Recipie+CoreDataProperties.m
//  CoreDataRelationsDemo
//
//  Created by James Cash on 25-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//  This file was automatically generated and should not be edited.
//

#import "Recipie+CoreDataProperties.h"

@implementation Recipie (CoreDataProperties)

+ (NSFetchRequest<Recipie *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Recipie"];
}

@dynamic steps;
@dynamic author;
@dynamic favouritedBy;

@end
