//
//  FavedViewController.m
//  CoreDataRelationsDemo
//
//  Created by James Cash on 25-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "FavedViewController.h"

@interface FavedViewController ()
@property (weak, nonatomic) IBOutlet UILabel *favedLabel;

@end

@implementation FavedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    self.favedLabel.text = [[self.favers allObjects] description];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
